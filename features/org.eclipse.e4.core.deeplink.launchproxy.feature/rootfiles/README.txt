In order for the launch proxy to work, the following configuration 
must be done:

The launch proxy is 100% self-contained inside its own directory 
structure, so installing and uninstalling it is simply a matter of 
copying it to the location of your choice. You can then register it 
with Windows so that it will automatically handle deeplink URLs 
that Windows encounters in desktop shortcuts or in web pages.

To register your launch proxy with Windows, you will need to add 
some keys to the Windows registry. A pre-built Windows registry 
configuration file is shipped with the launch proxy. You can edit 
this configuration file and load its keys as follows:

    * Locate the .reg file that was shipped with 
      launch proxy. You will find it in the root folder of the launch 
      proxy distribution.
    * Right-click this file, and choose Edit from the context menu.
    * Change the last line of the file to contain the full path 
      and file name of the launchr.exe file. You may optionally 
      specify an executable file containing an icon for deeplinks.
    * Save and exit the editor.
    * Double-click the .reg file to load it into Windows.

You have now installed the deeplink launch proxy and have 
configured Windows to understand the deeplink:// URL type.

For more details, please see the documentation at:

http://wiki.eclipse.org/E4/Deeplinking			
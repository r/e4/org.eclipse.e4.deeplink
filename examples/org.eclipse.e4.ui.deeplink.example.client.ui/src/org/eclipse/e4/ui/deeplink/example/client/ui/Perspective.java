/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.ui.deeplink.example.client.ui;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.e4.core.deeplink.api.AbstractDeepLinkInstanceHandler;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;


/**
 * An example Perspective that additionally defines a deeplink callback.
 * Since the Perspective Factory is already registerd with Eclipse, only have
 * to extend AbstractDeepLinkInstanceHandler to add the deeplink callback.
 * Deeplinking will automatically detect and call the method if it exists.
 */
public class Perspective extends AbstractDeepLinkInstanceHandler implements IPerspectiveFactory {

	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(false);
		layout.setFixed(true);
		
		layout.addStandaloneView(View.ID,  false, IPageLayout.LEFT, 1.0f, editorArea);
	}

	@Override
	public Map<String, String> activate(String handlerInstanceID,
			String action, Map<String, String[]> params) {
		HashMap<String, String> result = new HashMap<String, String>();
		result.put("date", new Date().toString());
		return result;
	}

}

/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.ui.deeplink.example.client;

import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

public class ApplicationActionBarAdvisor extends ActionBarAdvisor {

	private final IWorkbenchWindow workbenchWindow;
	
	private IWorkbenchAction quitAction;
	private IWorkbenchAction openPreferencesAction;
	private IWorkbenchAction newWindowAction;
	private IWorkbenchAction resetPerspectiveAction;
	private IWorkbenchAction introAction;
	private IWorkbenchAction aboutAction;
	private IWorkbenchAction showViewMenuAction;
    private IWorkbenchAction showHelpAction; // NEW
    private IWorkbenchAction searchHelpAction; // NEW
    private IWorkbenchAction dynamicHelpAction; // NEW

    public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
        super(configurer);
        workbenchWindow = configurer.getWindowConfigurer().getWindow();
    }

    protected void makeActions(IWorkbenchWindow window) {
		quitAction = ActionFactory.QUIT.create(window);
		register(quitAction);

    	openPreferencesAction = ActionFactory.PREFERENCES.create(window);
		register(openPreferencesAction);
    	
		newWindowAction = ActionFactory.OPEN_NEW_WINDOW.create(workbenchWindow);
		newWindowAction.setText("&New Window");
		register(newWindowAction);
    	
		resetPerspectiveAction = ActionFactory.RESET_PERSPECTIVE.create(window);
		resetPerspectiveAction.setText("Reset Window");
		register(resetPerspectiveAction);
		
		if (window.getWorkbench().getIntroManager().hasIntro()) {
			introAction = ActionFactory.INTRO.create(window);
			register(introAction);
		}
		
		aboutAction = ActionFactory.ABOUT.create(window);
		register(aboutAction);
		
		showViewMenuAction = ActionFactory.SHOW_VIEW_MENU.create(window);
		register(showViewMenuAction);

	    showHelpAction = ActionFactory.HELP_CONTENTS.create(window); // NEW
	    register(showHelpAction); // NEW

	    searchHelpAction = ActionFactory.HELP_SEARCH.create(window); // NEW
	    register(searchHelpAction); // NEW

	    dynamicHelpAction = ActionFactory.DYNAMIC_HELP.create(window); // NEW
	    register(dynamicHelpAction); // NEW
	}

    protected void fillMenuBar(IMenuManager menuBar) {
		menuBar.add(createFileMenu());    	
		menuBar.add(createWindowMenu());
		menuBar.add(createHelpMenu());

    }
    
	/**
	 * Creates and returns the File menu.
	 */
	private MenuManager createFileMenu() {
		MenuManager menu = new MenuManager("&File", IWorkbenchActionConstants.M_FILE);
		menu.add(new Separator());
		menu.add(quitAction);
		menu.add(new GroupMarker(IWorkbenchActionConstants.FILE_END));
		return menu;
	}    
	/**
	 * Creates and returns the Window menu.
	 */
	private MenuManager createWindowMenu() {
		final MenuManager menu = new MenuManager("&Window",
				IWorkbenchActionConstants.M_WINDOW);
		menu.add(newWindowAction);

		menu.add(resetPerspectiveAction);
		menu.add(new Separator());

		menu.add(showViewMenuAction);
		
		menu.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS + "end")); //NON-NLS-1
		
		menu.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		menu.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS + "end")); //NON-NLS-1
		menu.add(openPreferencesAction);
		
		return menu;
	}

	/**
	 * Creates and returns the Help menu.
	 */
	private MenuManager createHelpMenu() {
		MenuManager menu = new MenuManager("&Help",
				IWorkbenchActionConstants.M_HELP);
		addSeparatorOrGroupMarker(menu, "group.intro"); // NON-NLS-1
		// See if a welcome or intro page is specified
		if (introAction != null) {
			menu.add(introAction);
		}
		menu.add(new GroupMarker("group.intro.ext")); // NON-NLS-1
		menu.add(new Separator("group.about")); // NON-NLS-1
		menu.add(aboutAction);
		menu.add(new GroupMarker("group.about.ext")); // NON-NLS-1

		menu.add(showHelpAction); // NEW
		menu.add(searchHelpAction); // NEW
		menu.add(dynamicHelpAction); // NEW

		return menu;
	}
	
	/**
	 * Adds a <code>GroupMarker</code> or <code>Separator</code> to a menu.
	 * The test for whether a separator should be added is done by checking for
	 * the existence of a preference matching the string
	 * useSeparator.MENUID.GROUPID that is set to <code>true</code>.
	 * 
	 * @param menu
	 *            the menu to add to
	 * @param groupId
	 *            the group id for the added separator or group marker
	 */
	private void addSeparatorOrGroupMarker(MenuManager menu, String groupId) {
		String prefId = "useSeparator." + menu.getId() + "." + groupId; //NON-NLS-1 //NON-NLS-2
		menu.add(new GroupMarker(groupId));
	}
}
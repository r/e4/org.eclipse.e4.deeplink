/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.ui.deeplink.example.client;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.e4.core.deeplink.api.AbstractDeepLinkInstanceHandler;


/**
 * A simple deeplink callback handler that is registered via extension point. 
 */
public class SayHelloHandler extends AbstractDeepLinkInstanceHandler {
	
	@Override
	public Map<String, String> activate(String handlerInstanceID, String action, Map<String, String[]> params) {
		Map<String, String> results = new HashMap<String, String>();
		String helloMessage = "Hello, " + action;
		System.out.println(helloMessage);
		results.put("Hello", helloMessage);
		return results;
	}
}

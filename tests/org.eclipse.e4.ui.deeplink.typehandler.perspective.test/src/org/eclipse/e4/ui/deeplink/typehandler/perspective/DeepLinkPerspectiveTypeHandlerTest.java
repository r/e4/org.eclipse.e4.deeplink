/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.ui.deeplink.typehandler.perspective;

import org.easymock.EasyMock;
//import org.easymock.classextension.EasyMock;
import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.deeplink.api.IStatusFactory;
import org.eclipse.e4.ui.deeplink.typehandler.perspective.DeepLinkPerspectiveTypeHandler;
import org.eclipse.e4.ui.test.utils.SWTTestCase;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveRegistry;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPreferenceConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.WorkbenchException;


public class DeepLinkPerspectiveTypeHandlerTest extends SWTTestCase {

	public static final String PLUGIN_ID = "org.eclipse.e4.ui.deeplink.skeleton";
	
	private DeepLinkPerspectiveTypeHandler testee;
	
	public void setUp() throws Exception {
		super.setUp();
			
		testee = new DeepLinkPerspectiveTypeHandler();
		shell.setMinimized(true);
	}

	
	// ---------------------------------------------------------------------	
	
	private Status makeErrorStatus(String errorMessage) {
		Status errorStatus = new Status(Status.ERROR, PLUGIN_ID, errorMessage);
		IStatusFactory statusFactory = EasyMock.createMock(IStatusFactory.class);
		EasyMock.expect(statusFactory.error(errorMessage)).andReturn(errorStatus);
		EasyMock.replay(statusFactory);
		testee.statusFactory = statusFactory;
		return errorStatus;
	}
	
	private Status makeErrorStatus(String errorMessage, Throwable t) {
		Status errorStatus = new Status(Status.ERROR, PLUGIN_ID, errorMessage, t);
		IStatusFactory statusFactory = EasyMock.createMock(IStatusFactory.class);
		EasyMock.expect(statusFactory.error(errorMessage, t)).andReturn(errorStatus);
		EasyMock.replay(statusFactory);
		testee.statusFactory = statusFactory;
		return errorStatus;
	}
	
	private Status makeInfoStatus(String message) {
		Status status = new Status(Status.INFO, PLUGIN_ID, message);
		IStatusFactory statusFactory = EasyMock.createMock(IStatusFactory.class);
		EasyMock.expect(statusFactory.info(message)).andReturn(status);
		EasyMock.replay(statusFactory);
		testee.statusFactory = statusFactory;
		return status;
	}
	
	private ILog makeLogger(Status status) {
		ILog logger = EasyMock.createMock(ILog.class);
		logger.log(status);
		EasyMock.replay(logger);
		return logger;
	}
	
	private ILog makeLoggerExpectingError(String errorMessage) {
		Status errorStatus = makeErrorStatus(errorMessage);
		return makeLogger(errorStatus);
	}

	private ILog makeLoggerExpectingError(String errorMessage, Throwable t) {
		Status errorStatus = makeErrorStatus(errorMessage, t);
		return makeLogger(errorStatus);
	}

	private ILog makeLoggerExpectingInfo(String message) {
		Status status = makeInfoStatus(message);
		return makeLogger(status);
	}

	private IWorkbench makeWorkbenchWithPerspectiveID(String perspID, IPerspectiveDescriptor perspective) {
		IPerspectiveRegistry reg = EasyMock.createMock(IPerspectiveRegistry.class);
		EasyMock.expect(reg.findPerspectiveWithId(perspID)).andReturn(perspective);
		EasyMock.replay(reg);
		
		IWorkbench workbench = EasyMock.createMock(IWorkbench.class);
		EasyMock.expect(workbench.getPerspectiveRegistry()).andReturn(reg);
		
		return workbench;
	}

	private void expectWorkbenchWindowGetShell(IWorkbench workbench) {
		IWorkbenchWindow workbenchWindow = EasyMock.createMock(IWorkbenchWindow.class);
		EasyMock.expect(workbenchWindow.getShell()).andReturn(shell);
		EasyMock.expect(workbench.getActiveWorkbenchWindow()).andReturn(workbenchWindow);
		EasyMock.replay(workbenchWindow);
	}

	private IPreferenceStore makePreferenceStoreReturning(String returnValue) {
		IPreferenceStore store = EasyMock.createMock(IPreferenceStore.class);
		EasyMock.expect(store.getString(IWorkbenchPreferenceConstants.OPEN_NEW_PERSPECTIVE)).andReturn(returnValue);
		EasyMock.replay(store);
		return store;
	}
	
	//-----------------------------------------------------------------------
	
	public void testOpenPerspective_nonexistantPerspective_logsAndReturnsFalse() throws Exception {
		String perspID = "com.nonexistant.perspective";

		IWorkbench workbench = makeWorkbenchWithPerspectiveID(perspID, null);
		EasyMock.replay(workbench);
		
		// Expectations
		ILog logger = makeLoggerExpectingError("Unable to open perspective: " + perspID);
		
		boolean result = testee.openPerspective(perspID, null, workbench, null, logger);
		
		EasyMock.verify(logger);
		assertFalse(result);
	}
	
	public void testOpenPerspective_openInNewWindow_success() throws Exception {
		String perspID = "com.some.perspective";

		// Dependencies/prereqs
		IPreferenceStore store = makePreferenceStoreReturning(IWorkbenchPreferenceConstants.OPEN_PERSPECTIVE_WINDOW);
		IPerspectiveDescriptor perspective = EasyMock.createMock(IPerspectiveDescriptor.class);

		// Expectations
		ILog logger = makeLoggerExpectingInfo("Opened perspective: " + perspID);

		IWorkbench workbench = makeWorkbenchWithPerspectiveID(perspID, perspective);
		EasyMock.expect(workbench.openWorkbenchWindow(perspID, null)).andReturn(null);
		expectWorkbenchWindowGetShell(workbench);
		EasyMock.replay(workbench);
		
		boolean result = testee.openPerspective(perspID, null, workbench, store, logger);
		
		EasyMock.verify(workbench, logger);
		assertTrue(shell.isFocusControl());	// Prove the Shell got activated
		assertTrue(result);
	}

	public void testOpenPerspective_openInNewWindow_failure_logsErrorAndReturnsFalse() throws Exception {
		String perspID = "com.some.perspective";
		WorkbenchException e = new WorkbenchException("test error");

		// Dependencies/prereqs
		IPreferenceStore store = makePreferenceStoreReturning(IWorkbenchPreferenceConstants.OPEN_PERSPECTIVE_WINDOW);
		IPerspectiveDescriptor perspective = EasyMock.createMock(IPerspectiveDescriptor.class);

		// Expectations
		ILog logger = makeLoggerExpectingError("Error opening perspective: " + e.getMessage(), e);
		IWorkbench workbench = makeWorkbenchWithPerspectiveID(perspID, perspective);
		EasyMock.expect(workbench.openWorkbenchWindow(perspID, null)).andThrow(e);
		EasyMock.replay(workbench);
		
		boolean result = testee.openPerspective(perspID, null, workbench, store, logger);
		
		EasyMock.verify(workbench);
		EasyMock.verify(logger);
		assertFalse(result);
	}

	public void testOpenPerspective_openInCurrentWindowWithNewWorkbenchPage_failure_logsErrorAndReturnsFalse() throws Exception {
		String perspID = "com.some.perspective";
		WorkbenchException e = new WorkbenchException("test error");

		// Dependencies/prereqs
		IPreferenceStore store = makePreferenceStoreReturning(IWorkbenchPreferenceConstants.OPEN_PERSPECTIVE_REPLACE);
		IPerspectiveDescriptor perspective = EasyMock.createMock(IPerspectiveDescriptor.class);

		IWorkbenchWindow window = EasyMock.createMock(IWorkbenchWindow.class);
		EasyMock.expect(window.getActivePage()).andReturn(null);
		EasyMock.expect(window.openPage(perspID, null)).andThrow(e);
		EasyMock.replay(window);
		
		// Expectations
		ILog logger = makeLoggerExpectingError("Error opening perspective: " + e.getMessage(), e);

		IWorkbench workbench = makeWorkbenchWithPerspectiveID(perspID, perspective);
		EasyMock.expect(workbench.getActiveWorkbenchWindow()).andReturn(window);
		EasyMock.replay(workbench);
		
		boolean result = testee.openPerspective(perspID, null, workbench, store, logger);
		
		EasyMock.verify(workbench, window, logger);
		assertFalse(result);
	}

	public void testOpenPerspective_openInCurrentWindowWithNewWorkbenchPage_success_ReturnsTrueAndActivatesShell() throws Exception {
		String perspID = "com.some.perspective";

		// Dependencies/prereqs
		IPreferenceStore store = makePreferenceStoreReturning(IWorkbenchPreferenceConstants.OPEN_PERSPECTIVE_REPLACE);
		IPerspectiveDescriptor perspective = EasyMock.createMock(IPerspectiveDescriptor.class);

		IWorkbenchWindow window = EasyMock.createMock(IWorkbenchWindow.class);
		EasyMock.expect(window.getActivePage()).andReturn(null);
		EasyMock.expect(window.openPage(perspID, null)).andReturn(null);
		EasyMock.expect(window.getShell()).andReturn(shell);
		EasyMock.replay(window);
		
		// Expectations
		ILog logger = makeLoggerExpectingInfo("Opened perspective: " + perspID);
		
		IWorkbench workbench = makeWorkbenchWithPerspectiveID(perspID, perspective);
		EasyMock.expect(workbench.getActiveWorkbenchWindow()).andReturn(window);
		EasyMock.expect(workbench.getActiveWorkbenchWindow()).andReturn(window);
		EasyMock.replay(workbench);
		
		boolean result = testee.openPerspective(perspID, null, workbench, store, logger);
		
		EasyMock.verify(workbench, window, logger);
		assertTrue(shell.isFocusControl());	// Prove the Shell got activated
		assertTrue(result);
	}

	public void testOpenPerspective_openInCurrentWindowWithExistingWorkbenchPage_success_ReturnsTrueAndActivatesShell() throws Exception {
		String perspID = "com.some.perspective";

		// Dependencies/prereqs
		IPreferenceStore store = makePreferenceStoreReturning(IWorkbenchPreferenceConstants.OPEN_PERSPECTIVE_REPLACE);
		IPerspectiveDescriptor perspective = EasyMock.createMock(IPerspectiveDescriptor.class);

		IWorkbenchPage page = EasyMock.createMock(IWorkbenchPage.class);
		page.setPerspective(perspective);
		EasyMock.replay(page);
		
		IWorkbenchWindow window = EasyMock.createMock(IWorkbenchWindow.class);
		EasyMock.expect(window.getActivePage()).andReturn(page);
		EasyMock.expect(window.getShell()).andReturn(shell);
		EasyMock.replay(window);
		
		// Expectations
		ILog logger = makeLoggerExpectingInfo("Opened perspective: " + perspID);
		
		IWorkbench workbench = makeWorkbenchWithPerspectiveID(perspID, perspective);
		EasyMock.expect(workbench.getActiveWorkbenchWindow()).andReturn(window);
		EasyMock.expect(workbench.getActiveWorkbenchWindow()).andReturn(window);
		EasyMock.replay(workbench);
		
		boolean result = testee.openPerspective(perspID, null, workbench, store, logger);
		
		EasyMock.verify(workbench, window, page, logger);
		assertTrue(shell.isFocusControl());	// Prove the Shell got activated
		assertTrue(result);
	}

}

/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.core.deeplink.internal;

import org.eclipse.e4.core.deeplink.DeepLinkURLException;
import org.eclipse.e4.core.deeplink.internal.ParsedDeepLinkURL;


import junit.framework.TestCase;

public class ParsedDeepLinkURLTest extends TestCase {
	public void testCtor_InvalidProtocol_failure() throws Exception {
		try {
			new ParsedDeepLinkURL("foo://bar/baz");
			fail("Expected exception on invalid URL");
		} catch (DeepLinkURLException e) {
			//success
		}
	}

	public void testCtor_protocolPresent_success() throws Exception {
		try {
			new ParsedDeepLinkURL("deeplink://bar/baz");
			//success
		} catch (DeepLinkURLException e) {
			fail("Expected exception on invalid URL");
		}
	}

	public void testCtor_InvalidInstallation_failure() throws Exception {
		try {
			new ParsedDeepLinkURL("deeplink://");
			fail("Expected exception on invalid URL");
		} catch (DeepLinkURLException e) {
			//success
		}
	}

	public void testCtor_InstallationPresent_success() throws Exception {
		try {
			new ParsedDeepLinkURL("deeplink://bar/baz");
			//success
		} catch (DeepLinkURLException e) {
			fail("Expected exception on invalid URL");
		}
	}
	
	public void testCtor_InstallationIsJustSlashes_failure() throws Exception {
		try {
			new ParsedDeepLinkURL("deeplink:///");
			fail("Expected exception on invalid URL");
		} catch (DeepLinkURLException e) {
			//success
		}
	}
	
	public void testCtor_setsInstallation() throws Exception {
		final String INSTALLATION="installation";
		ParsedDeepLinkURL testee = new ParsedDeepLinkURL("deeplink://" + INSTALLATION);
		assertEquals(INSTALLATION, testee.installation);
	}
	
	public void testCtor_installationDoesNotIncludeRestOfURL() throws Exception {
		final String INSTALLATION="installation";
		final String REST="/restOfURL";
		ParsedDeepLinkURL testee = new ParsedDeepLinkURL("deeplink://" + INSTALLATION + REST);
		assertEquals(INSTALLATION, testee.installation);
	}
	
	public void testCtor_RestOfURLIsEmpty() throws Exception {
		final String INSTALLATION="installation";
		final String REST="";
		ParsedDeepLinkURL testee = new ParsedDeepLinkURL("deeplink://" + INSTALLATION + REST);
		assertEquals(REST, testee.restOfURL);
	}

	public void testCtor_RestOfURLIsSlash() throws Exception {
		final String INSTALLATION="installation";
		final String REST="/";
		ParsedDeepLinkURL testee = new ParsedDeepLinkURL("deeplink://" + INSTALLATION + REST);
		assertEquals(REST, testee.restOfURL);
	}

	public void testCtor_RestOfURLIsNotEmpty() throws Exception {
		final String INSTALLATION="installation";
		final String REST="/restOfURL";
		ParsedDeepLinkURL testee = new ParsedDeepLinkURL("deeplink://" + INSTALLATION + REST);
		assertEquals(REST, testee.restOfURL);
	}
	
	public void testCtor_installationDuplicatedInsideURL_restOfURLIsParsedCorrectly() throws Exception {
		final String INSTALLATION="installation";
		final String REST="/restOfURL/foo/bar/installation/baz";
		ParsedDeepLinkURL testee = new ParsedDeepLinkURL("deeplink://" + INSTALLATION + REST);
		assertEquals(REST, testee.restOfURL);
	}
	
	
}

/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.core.deeplink.internal;


import org.eclipse.e4.core.deeplink.internal.DeepLinkProperties;
import org.eclipse.e4.core.deeplink.internal.DeeplinkPortAssigner;


import junit.framework.TestCase;

public class DeeplinkPortAssignerTest extends TestCase {
	
	private static final String BASE_PORT_NUMBER_KEY = "base.port.number";
	private static final int BASE_PORT_NUMBER = 9000;
	private static final String MY_INSTALLATION = "myInstallation";
	
	private DeepLinkProperties props;

	public void setUp() throws Exception {	
		props = new DeepLinkProperties();
		props.put(BASE_PORT_NUMBER_KEY, BASE_PORT_NUMBER);
	}
	

	public void testPortNumberIsNotRegeneratedWhenRegeneratePortNumberIsFalse_andThrowsException() {
		props.setInstallationPort("oneInstallation", BASE_PORT_NUMBER+2);
		props.setInstallationPort(MY_INSTALLATION, BASE_PORT_NUMBER+2);
		DeeplinkPortAssigner testee = new DeeplinkPortAssigner(props, false);

		try{
			testee.getPortNumberForInstallation(MY_INSTALLATION);
			fail();
		} catch (RuntimeException e) {
			// pass();
		}
		
	}
	
	
	public void testPortForInstallationIsRegenerated_whenTheInstallationsPortIsAlreadyAssignedInTheProperties(){
		props.setInstallationPort("oneInstallation", BASE_PORT_NUMBER+2);
		props.setInstallationPort(MY_INSTALLATION, BASE_PORT_NUMBER+2);
		DeeplinkPortAssigner testee = new DeeplinkPortAssigner(props, true);
		
		assertEquals(BASE_PORT_NUMBER, testee.getPortNumberForInstallation(MY_INSTALLATION));
		assertEquals(Integer.toString(BASE_PORT_NUMBER), props.getInstallationPort(MY_INSTALLATION));
	}
	
	public void testPortForInstallationIsGenereatedAndStored_whenRequestedInstallationDoesNotExist_andTwoExistingInstallationExistAlready() {
		props.setProperty("yetAnotherInstallation", Integer.toString(BASE_PORT_NUMBER+2) );
		DeeplinkPortAssigner testee = new DeeplinkPortAssigner(props, true);
		assertEquals(BASE_PORT_NUMBER, testee.getPortNumberForInstallation("someInstallation"));
		assertEquals(BASE_PORT_NUMBER+1, testee.getPortNumberForInstallation("moreInstallations"));
		assertEquals(BASE_PORT_NUMBER+3, testee.getPortNumberForInstallation(MY_INSTALLATION));
	}
	
	public void testWhenPropsIsNull_RuntimeExceptionThrows() {		
		try {
			new DeeplinkPortAssigner(null, true);
			fail();
		} catch (IllegalArgumentException e) {
			// pass();
		}		
	}
	
	public void testPortForInstallationIsGenereatedAndStored_whenRequestedInstallationDoesNotExist_andAnExistingInstallationHasBasePortNumber() {

		DeeplinkPortAssigner testee = new DeeplinkPortAssigner(props, true);
		assertEquals(BASE_PORT_NUMBER, testee.getPortNumberForInstallation("someInstallation"));
		assertEquals(BASE_PORT_NUMBER+1, testee.getPortNumberForInstallation(MY_INSTALLATION));		
	}
	
	public void testPortForInstallationIsGeneratedAndStored_whenInstallationDoesNotExistInProps() {					
		DeeplinkPortAssigner testee = new DeeplinkPortAssigner(props, true); 		
		assertEquals(BASE_PORT_NUMBER, testee.getPortNumberForInstallation(MY_INSTALLATION));		
		assertEquals(Integer.toString(BASE_PORT_NUMBER), props.getInstallationPort(MY_INSTALLATION));		
	}
	
	public void testPortForInstallationIsGeneratedAndStored_whenInstallationDoesNotExistInPropsAndPropsDoesNotHaveBasePortNumber() {					
		props = new DeepLinkProperties();
		DeeplinkPortAssigner testee = new DeeplinkPortAssigner(props, true); 		
		assertEquals(DeepLinkProperties.DEFAULT_BASE_PORT_NUMBER, testee.getPortNumberForInstallation(MY_INSTALLATION));		
		assertEquals(Integer.toString(DeepLinkProperties.DEFAULT_BASE_PORT_NUMBER), props.getInstallationPort(MY_INSTALLATION));		
	}		
	
	public void testPortForInstallationReadFromProperties_whenInstallationAlreadyExistsInProps() {		
		int expectedPortNumber = 8888;
		props.setInstallationPort(MY_INSTALLATION, expectedPortNumber);
		
		DeeplinkPortAssigner testee = new DeeplinkPortAssigner(props, true); 
		
		assertEquals(expectedPortNumber, testee.getPortNumberForInstallation(MY_INSTALLATION));		
	}
	
	

}

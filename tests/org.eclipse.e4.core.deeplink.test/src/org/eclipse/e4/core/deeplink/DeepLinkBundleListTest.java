/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.core.deeplink;

import junit.framework.TestCase;

import org.easymock.EasyMock;
import org.eclipse.e4.core.deeplink.DeepLinkBundleList;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;


public class DeepLinkBundleListTest extends TestCase {
	private DeepLinkBundleList testee = null;
	
	// Utility methods ------------------------------------------------------------
	
	private DeepLinkBundleList newStartupBundlesHack(Bundle[] availableBundles) throws Exception {
		BundleContext context = EasyMock.createMock(BundleContext.class);
		EasyMock.expect(context.getBundles()).andStubReturn(availableBundles);
		EasyMock.replay(context);
		return new DeepLinkBundleList(context);
	}
	
	private Bundle newUnstartableBundle(String bundleName) {
		Bundle bundle = EasyMock.createMock(Bundle.class);
		EasyMock.expect(bundle.getSymbolicName()).andStubReturn(bundleName);
		EasyMock.replay(bundle);
		return bundle;
	}

	private Bundle newBundleToStart(String bundleName) throws BundleException {
		Bundle bundle = EasyMock.createMock(Bundle.class);
		EasyMock.expect(bundle.getSymbolicName()).andStubReturn(bundleName);
		bundle.stop();
		bundle.start();
		EasyMock.replay(bundle);
		return bundle;
	}

	// Verify that we started the Bundles we wanted to start and no others
	private void verifyBundles(Bundle[] availableBundles) {
		for (Bundle bundle : availableBundles) {
			EasyMock.verify(bundle);
		}
	}

	// Tests ----------------------------------------------------------------------
	
	/*
	 * There's nothing sacred about the set of bundles to start.  But we do want the
	 * test to notice if this set changes.
	 */
	public void testExpectedSetOfStartupBundles() throws Exception {
		testee = newStartupBundlesHack(new Bundle[]{});
		String[] bundlesToLoad = testee.loadTheseBundles;
		assertEquals(4, bundlesToLoad.length);
		assertEquals("org.eclipse.e4.core.deeplink.handler", bundlesToLoad[0]);
		assertEquals("org.eclipse.equinox.http.jetty", bundlesToLoad[1]);
		assertEquals("org.eclipse.equinox.http.servlet", bundlesToLoad[2]);
		assertEquals("org.eclipse.equinox.http.registry", bundlesToLoad[3]);
	}

	public void testStartupBundlesForHTTPServlets_noneMatch() throws Exception {
		Bundle[] availableBundles = new Bundle[] {
				newUnstartableBundle("a"),
				newUnstartableBundle("b"),
				newUnstartableBundle("c")
		};
		testee = newStartupBundlesHack(availableBundles);
		
		testee.startupBundlesForHttpServlets();

		verifyBundles(availableBundles);
	}

	public void testStartupBundlesForHTTPServlets_allMatch() throws Exception {
		Bundle[] availableBundles = new Bundle[] {
				newBundleToStart("org.eclipse.e4.core.deeplink.handler"),
				newBundleToStart("org.eclipse.equinox.http.jetty"),
				newBundleToStart("org.eclipse.equinox.http.servlet"),
				newBundleToStart("org.eclipse.equinox.http.registry")
		};
		testee = newStartupBundlesHack(availableBundles);
		
		testee.startupBundlesForHttpServlets();

		verifyBundles(availableBundles);
	}

	public void testStartupBundlesForHTTPServlets_someMatch_throwsException() throws Exception {
		Bundle[] availableBundles = new Bundle[] {
				newUnstartableBundle("a"),
				newUnstartableBundle("b"),
				newUnstartableBundle("c"),
				newBundleToStart("org.eclipse.e4.core.deeplink.handler"),
//				newBundleToStart("org.eclipse.equinox.http.jetty"),  // commenting this out should cause exception below
				newBundleToStart("org.eclipse.equinox.http.servlet"),
				newBundleToStart("org.eclipse.equinox.http.registry")
		};
		testee = newStartupBundlesHack(availableBundles);

		try {
			testee.startupBundlesForHttpServlets();
			// If the deep link handler is present, jetty, http.servlet, and http.reg must also be
			fail("Should have detected that jetty is not available to start");
		} catch (BundleException e) {
			//success
		}
	}
	
	public void testStartupBundlesForHTTPServlets_someMatch_butNoDeepLink() throws Exception {
		Bundle[] availableBundles = new Bundle[] {
				newUnstartableBundle("a"),
				newUnstartableBundle("b"),
				newUnstartableBundle("c"),
// The following two are intentionally commented out:
//				newBundleToStart("org.eclipse.e4.core.deeplink.handler"),
//				newBundleToStart("org.eclipse.equinox.http.jetty"),
				newBundleToStart("org.eclipse.equinox.http.servlet"),
				newBundleToStart("org.eclipse.equinox.http.registry")
		};
		testee = newStartupBundlesHack(availableBundles);

		try {
			testee.startupBundlesForHttpServlets();
			verifyBundles(availableBundles);
			// success because we only enforce all/nothing if deeplink is specified
		} catch (BundleException e) {
			fail("Deep linking isn't available; client may specify anything else");
		}
	}
	
}

/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.core.deeplink.handler;

import junit.framework.TestCase;

//import org.easymock.classextension.EasyMock;
import org.easymock.EasyMock;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.e4.core.deeplink.api.Activator;
import org.eclipse.e4.core.deeplink.handler.DeepLinkTypeHandlerMapper;


public class DeepLinkTypeHandlerMapperTest extends TestCase {
	
	private DeepLinkTypeHandlerMapper testee;
	private IExtensionRegistry mockRegistry = EasyMock.createMock(IExtensionRegistry.class);;
	
	public void setUp() {
		
	}

	// TODO: Should this fail-fast instead ??? 
	public void testEmptyMapReturnedWhenNoExtensionsFound() {
		EasyMock.expect(mockRegistry.getConfigurationElementsFor(Activator.PLUGIN_ID,
				Activator.DEEP_LINK_EXT_PT_ID)).andReturn(new IConfigurationElement[]{});
		EasyMock.replay(mockRegistry);
		
		testee = new DeepLinkTypeHandlerMapper(mockRegistry);
		assertEquals(0, testee.getMap().size());
		
		EasyMock.verify(mockRegistry);		
	}
	
	public void testRunTimeExceptionThrownWhenExtensionRegistryIsNull() {
				
		try {
			testee = new DeepLinkTypeHandlerMapper(null);
			fail();
		} catch (IllegalArgumentException e) {
			//pass();
		}
		
	}
	
}

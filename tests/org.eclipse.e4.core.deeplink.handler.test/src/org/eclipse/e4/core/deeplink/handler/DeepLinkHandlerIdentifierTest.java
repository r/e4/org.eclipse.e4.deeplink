/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.core.deeplink.handler;

import org.eclipse.e4.core.deeplink.api.URLPathInfoParser;

import junit.framework.TestCase;

public class DeepLinkHandlerIdentifierTest extends TestCase {

	public void testDeepLinkHandlerIdentifier_nullInput_throwsRuntimeException() throws Exception {
		try {
			new URLPathInfoParser(null);
			fail("Expected an IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			// success
		}
	}
	
	public void testDeepLinkHandlerIdentifier_nonNullInput_success() throws Exception {
		try {
			new URLPathInfoParser("/foo/bar");
			// success
		} catch (IllegalArgumentException e) {
			fail("Did not expect an IllegalArgumentException");
		}
	}
	
	public void testDeepLinkHandlerIdentifier_onlyForwardSlash_throwsRuntimeException() throws Exception {
		try {
			new URLPathInfoParser("/");
			fail("Expected an IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			// success
		}
	}
	
	public void testDeepLinkHandlerIdentifier_fails_whenOnlyDeepLinkTypePresent() throws Exception {
		final String DEEP_LINK_TYPE = "deepLinkType";
		try {
			new URLPathInfoParser("/" + DEEP_LINK_TYPE);
			fail("Expected IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			//success
		}
	}

	public void testDeepLinkHandlerIdentifier_parses_whenDeepLinkTypeAndPartIDPresent() throws Exception {
		final String DEEP_LINK_TYPE = "deepLinkType";
		final String PART_ID = "partID";
		URLPathInfoParser testee = new URLPathInfoParser("/" + DEEP_LINK_TYPE + "/" + PART_ID);
		assertEquals(DEEP_LINK_TYPE, testee.handlerType);
		assertEquals(PART_ID, testee.handlerId);
		assertFalse(testee.action.hasValue());
	}

	public void testDeepLinkHandlerIdentifier_parses_whenDeepLinkTypeAndPartIDandHandlerPresent() throws Exception {
		final String DEEP_LINK_TYPE = "deepLinkType";
		final String PART_ID = "partID";
		final String HANDLER = "handler";
		URLPathInfoParser testee = new URLPathInfoParser("/" + DEEP_LINK_TYPE + "/" + PART_ID + "/" + HANDLER);
		assertEquals(DEEP_LINK_TYPE, testee.handlerType);
		assertEquals(PART_ID, testee.handlerId);
		assertEquals(HANDLER, testee.action.get());
	}

	public void testDeepLinkHandlerIdentifier_parses_withInnumerableSlashes() throws Exception {
		final String DEEP_LINK_TYPE = "deepLinkType";
		final String PART_ID = "partID";
		final String HANDLER = "handler";
		URLPathInfoParser testee = new URLPathInfoParser("/" + DEEP_LINK_TYPE + "/" + PART_ID + "////////////////////////////////////////////////////////////////////////" + HANDLER);
		assertEquals(DEEP_LINK_TYPE, testee.handlerType);
		assertEquals(PART_ID, testee.handlerId);
		assertEquals(HANDLER, testee.action.get());
	}
}

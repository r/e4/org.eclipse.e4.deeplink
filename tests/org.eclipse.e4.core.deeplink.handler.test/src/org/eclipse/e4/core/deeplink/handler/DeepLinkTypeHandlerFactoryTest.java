/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.core.deeplink.handler;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import org.easymock.EasyMock;
//import org.easymock.classextension.EasyMock;
import org.eclipse.e4.core.deeplink.api.AbstractDeepLinkTypeHandler;
import org.eclipse.e4.core.deeplink.handler.DeepLinkTypeHandlerFactory;
import org.eclipse.e4.core.functionalprog.optionmonad.Option;


public class DeepLinkTypeHandlerFactoryTest extends TestCase {

	private DeepLinkTypeHandlerFactory testee;
	private Map<String, AbstractDeepLinkTypeHandler> typeHandlerMap;
	
	public void setUp()	{
		typeHandlerMap = new HashMap<String, AbstractDeepLinkTypeHandler>();
		testee = new DeepLinkTypeHandlerFactory(typeHandlerMap);
	}

	public void testNullInput_ExpectRuntimeException() throws Exception {
		HttpServletRequest request = makeServletRequest(null);
		
		try {
			testee.createTypeHandler(request, null);			
			fail("Expected IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			//success
		}
	}
	
	public void testExtensionURL_ReturnsATypeHandler() throws Exception {
		final String HANDLER_TYPE = "extensionType";
		
		//dependencies
		String urlPathInfo = "/" + HANDLER_TYPE + "/extensionId";
		HttpServletRequest request = makeServletRequest(urlPathInfo);
		HttpServletResponse response = makeServletResponse();

		//expectations
		DeepLinkTypeHandlerStub expectedTypeHandler = new DeepLinkTypeHandlerStub();
		typeHandlerMap.put(HANDLER_TYPE, expectedTypeHandler);

		Option<AbstractDeepLinkTypeHandler> result = testee.createTypeHandler(request, response);

		assertTrue("Found expected type handler", result.get() == expectedTypeHandler);
	}

	public void testUnableToIdentifyTypeOfLink_returnsNone() throws Exception {
		HttpServletRequest request = makeServletRequest("/noSuchThing/someId");
		HttpServletResponse response = null;
		
		Option<AbstractDeepLinkTypeHandler> result = testee.createTypeHandler(request, response);
		assertTrue("Handler should have returned None", !result.hasValue());
	}

	private HttpServletResponse makeServletResponse() {
		HttpServletResponse response = EasyMock.createNiceMock(HttpServletResponse.class);
		response.setContentType("text/xml");
		EasyMock.replay(response);
		return response;
	}

	private HttpServletRequest makeServletRequest(String urlPathInfo)
			throws UnsupportedEncodingException {
		HttpServletRequest request = EasyMock.createNiceMock(HttpServletRequest.class);
		request.setCharacterEncoding("UTF-8");		
		EasyMock.expect(request.getPathInfo()).andStubReturn(urlPathInfo);
		EasyMock.replay(request);
		return request;
	}
}

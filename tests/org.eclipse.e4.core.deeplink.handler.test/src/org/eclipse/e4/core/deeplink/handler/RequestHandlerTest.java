/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.core.deeplink.handler;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import org.easymock.EasyMock;
import org.eclipse.e4.core.deeplink.api.AbstractDeepLinkTypeHandler;
import org.eclipse.e4.core.deeplink.handler.RequestHandler;

public class RequestHandlerTest extends TestCase {
	
	private RequestHandler testee;
	private HttpServletRequest request;
	private HttpServletResponse response;

	public void setUp() throws Exception {
		testee = new RequestHandler(new HashMap<String, AbstractDeepLinkTypeHandler>());	
		
		request = EasyMock.createMock(HttpServletRequest.class);				
		EasyMock.expect(request.getRemoteHost()).andReturn("thisIsNotLocalhost");				
		EasyMock.replay(request);
		
		response = EasyMock.createMock(HttpServletResponse.class);		
		response.sendError(HttpServletResponse.SC_FORBIDDEN);		
		EasyMock.replay(response);
	}
	
	public void testRequestHandlerDoesNotProcessGetRequestsFromNonLocalMachines() throws ServletException, IOException {
		testee.doGet(request, response);
		EasyMock.verify(response);
	}
	
	public void testRequestHandlerDoesNotProcessPutRequestsFromNonLocalMachines() throws ServletException, IOException {					
		testee.doPost(request, response);
		EasyMock.verify(response);
	}
	
}

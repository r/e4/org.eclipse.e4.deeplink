/******************************************************************************
 * Copyright (c) David Orme and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Orme - initial API and implementation
 ******************************************************************************/
package org.eclipse.e4.core.deeplink.handler;

import java.io.IOException;

import org.eclipse.e4.core.deeplink.api.AbstractDeepLinkTypeHandler;


public class DeepLinkTypeHandlerStub extends AbstractDeepLinkTypeHandler {

	@Override
	public void processDeepLink() throws IOException {
		throw new UnsupportedOperationException();
	}
}
